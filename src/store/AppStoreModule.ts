import { StoreModule } from '@ngrx/store';

// eslint-disable-next-line @typescript-eslint/naming-convention
export const AppStoreModule = [StoreModule.forRoot([])];
