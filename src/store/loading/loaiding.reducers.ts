import { createReducer, on } from '@ngrx/store';
import { hide, show } from './loading.action';

const reducer = createReducer(
  {},
  on(show, () => ({})),
  on(hide, () => ({}))
);

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function loadingReducer(state, action) {
    return reducer(state, action);
}
