import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/product/products.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
  datacategory: any;
  id: any;
  constructor(private categoryService: ProductsService, private route: ActivatedRoute) {
    this.id = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.listCategory();
    this.getcoba();
  }
  listCategory() {
    setTimeout(() => {
      this.categoryService.listCategory().subscribe((data) => {
        this.datacategory = data;
        console.log(this.datacategory);
      });
    }, 5000);
  }
  getexmapleapi() {
    return this.categoryService.getexmapleapi().subscribe((data) => {
      console.log(data);
    });
  }
  getcoba(){
    return this.categoryService.productcobaget().subscribe((data) => {
      console.log(data);
    });
  }
}
