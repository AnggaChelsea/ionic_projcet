import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/product/products.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  tabs: { tab: string };
  target: HTMLIonTabsElement;
  dataProduct: any;
  thislike: any;
  constructor(private product: ProductsService, private http: HttpClient) {}

  ngOnInit() {
    this.getProduct();
  }

  getProduct() {
    setTimeout(() => {
      this.product.getProducts().subscribe((res) => {
        this.dataProduct = res.data;
        console.log(this.dataProduct);
      });
    }, 1000);
  }

  // likeProiduct() {
  //   return this.http
  //     .post<any>(
  //       `https://arcane-tor-29221.herokuapp.com/products/like/${this.dataProduct.id}`
  //     )
  //     .subscribe((res) => {
  //       this.thislike = res;
  //       console.log(this.thislike);
  //     });
  // }
}
