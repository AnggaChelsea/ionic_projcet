import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/product/products.service';
@Component({
  selector: 'app-jual',
  templateUrl: './jual.component.html',
  styleUrls: ['./jual.component.scss'],
})
export class JualComponent implements OnInit {
  detail: { login: string };
  target: HTMLIonTabsElement;
  dataCategory: any;
  addValueProduct = {
    seller: '620a46968b9afa12c935dacc', //this.datalocalstorage.getItem('id'),
    name: '',
    description: '',
    richDescription: '',
    image: '',
    images: [],
    brand: '',
    price: '',
    category: '',
    countInStock: '',
    rating: 0,
  };
  constructor(private productService: ProductsService) {}

  ngOnInit() {
    this.getCategory();
  }

  addProduct(product: any) {
    this.productService.addProduct(this.addValueProduct).subscribe((data) => {
      console.log(data);
      alert('berhasil');
    });
  }
  getCategory() {
    this.productService.listCategory().subscribe((data) => {
      console.log(data);
      this.dataCategory = data;
    });
  }
}
