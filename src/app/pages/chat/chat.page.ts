import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  title = { tab: '' };
  datauser: any;
  message: any;
  userIdStorages: any;
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.userIdStorages = localStorage.getItem('userId');
    this.datauser = this.getUserChat();
    this.message = this.getUserChat();
  }

  ionViewWillEnter() {
    this.title = { tab: '/' };
  }

  getUserChat() {
    // eslint-disable-next-line max-len
    return this.http
      .get<any>(
        `https://arcane-tor-29221.herokuapp.com/inbox/getInboxByUserId/${this.userIdStorages}`
      )
      .subscribe((data: any) => {
        if (data.message === 'successfully') {
          this.datauser = data.data;
          console.log(this.datauser);
        }
      });
  }
}
