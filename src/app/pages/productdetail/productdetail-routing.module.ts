import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PesanPage } from '../pesan/pesan.page';
import { ProductdetailPage } from './productdetail.page';

const routes: Routes = [
  {
    path: '',
    component: ProductdetailPage,
  },
  {
    path: 'pesan',
    component:PesanPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ProductdetailPageRoutingModule {}
