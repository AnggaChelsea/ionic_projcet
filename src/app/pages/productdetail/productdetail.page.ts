import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ProductsService } from '../../services/product/products.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.page.html',
  styleUrls: ['./productdetail.page.scss'],
})
export class ProductdetailPage implements OnInit {
  dataapi = 'https://arcane-tor-29221.herokuapp.com/products/productidby/';
  detail: { tab: string };
  target: HTMLIonTabsElement;
  getdata: any = {};
  productId: any;
  detailnya: any;
  seller: any;
  constructor(
    private nav: NavController,
    private productS: ProductsService,
    private actRoute: ActivatedRoute,
    private http: HttpClient
  ) {
    this.productId = this.actRoute.snapshot.params.id;
  }

  ngOnInit() {
    this.getByCategory();
    this.detailnya = this.getDetaipProduct();
    this.actRoute.paramMap.subscribe((params) => {
      this.productId = params.get('id');
      console.log('ini id', this.productId);
    });
    console.log('ini id', this.productId);
    // this.getByCategory(this.actRoute.snapshot.params.id);
    // console.log(
    //   'ini atas',
    //   this.getByCategory(this.actRoute.snapshot.params.id)
    // );
  }
  getByCategory() {
    return this.productS
      .getProductByCategory()
      .subscribe((data) => {
        this.getdata = data;
        console.log('ini bawah', this.getdata);
      });
  }
  getDetaipProduct(){
    return this.http.get<any>(`https://arcane-tor-29221.herokuapp.com/products/productidby/${this.productId}`).subscribe((data) => {
      this.detailnya = data.data;
      this.seller = data.seller;
      console.log('ini detail', this.detailnya);
    });
  }
}
