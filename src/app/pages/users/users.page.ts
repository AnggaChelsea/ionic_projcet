import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/users/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  userIdStorages: any;
  datauser: any;
  urlProductService: 'https://arcane-tor-29221.herokuapp.com/products/getProductByUser/';
  constructor(
    private userservice: UserService,
    private actRoute: ActivatedRoute,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.userIdStorages = localStorage.getItem('userId');
    console.log(this.userIdStorages);
    this.getDataIdUser();
  }
  getDataIdUser() {
    // eslint-disable-next-line max-len
    return this.http
      .get<any>(
        `https://arcane-tor-29221.herokuapp.com/products/getProductByUser/${this.userIdStorages}`
      )
      .subscribe((data: any) => {
        this.datauser = data;
        console.log(this.datauser);
        const datas = this.datauser.length;
        for (const valueuser of datas) {
          if (valueuser.seller != null) {
            console.log(valueuser);
          }
        }
      });
  }

  register() {}
}
