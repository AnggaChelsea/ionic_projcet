import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/product/products.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-productcategory',
  templateUrl: './productcategory.page.html',
  styleUrls: ['./productcategory.page.scss'],
})
export class ProductcategoryPage implements OnInit {
  public dataProduct: any;
  sampledata: any = {};
  dataId: any;
  id: any;
  constructor(
    private productservice: ProductsService,
    private activateRoute: ActivatedRoute,
    private http: HttpClient
  ) {
    this.id = this.activateRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getDataProductById();
    console.log(this.getDataProductById());
    this.id = this.activateRoute.snapshot.paramMap.get('id');
    console.log('bisabisa', this.id);
    // this.getDataProductById(this.id);
    // console.log(this.getDataProductById(this.id));
    // this.getCoba();
  }

  getDataProductById() {
    this.http
      .get<any>(
        `https://arcane-tor-29221.herokuapp.com/products/filterbyCategory/${this.id}`
      )
      .subscribe((res) => {
        this.dataProduct = res;
        console.log('ini data product', this.dataProduct);
        for(const data of this.dataProduct){
          console.log('ini data', data.name);
        }
        //import data this.dataProduct to component html template
      });
  }
  xampleCat(_id: any) {
    setTimeout(() => {
      this.productservice.exdatabycategory('id').subscribe((res) => {
        this.sampledata = res;
      });
    }, 10000);
  }
  getCoba() {
    return this.productservice.productcobaget().subscribe((res) => {
      console.log(res);
    });
  }

  getLike() {
    alert("you like this")
  }
}
