import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/users/user.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  detail: { login: string };
  target: HTMLIonTabsElement;
  getDataId: any;
  getdata: any;
  loginValue = {
    email: '',
    password: '',
  };
  constructor(
    private auth: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getDataId = this.route.snapshot.paramMap.get('id');
    console.log(this.getDataId);
  }

  loginbygoogle() {
    alert('google');
  }

  loginbyfacebook() {
    alert('facebook');
  }

  loginbyemail(user: any) {
    this.auth.login(this.loginValue).subscribe((data) => {
      this.getdata = data;
      console.log(this.getdata);
      localStorage.setItem('token', this.getdata.token);
      localStorage.setItem('userId', this.getdata.id);
      this.router.navigate([`/profile/${this.getdata.id}`]);
    });
  }
}
