import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/users/user.service';
import { GeolocationService } from '@ng-web-apis/geolocation';
import {  FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  registerinterface = {
    name: '',
    email: '',
    password: '',
  };
  profileForm = new FormGroup({
    name: new FormControl(''),
  email: new FormControl(''),
  password: new FormControl(''),

  });
  buttonHidden = false;

  constructor(
    private router: Router,
    private userService: UserService,
    private readonly geolocation$: GeolocationService
  ) {}

  ngOnInit() {
    // this.geolocation$.subscribe((geolocation) => {
    //   console.log(geolocation);
    // });
  }

  // registration() {
  //   this.userService.registerUser(this.profileForm.value).subscribe((res) => {
  //     console.log(res);
  //     console.log(this.profileForm);
  //     confirm('success registration');
  //     this.router.navigate(['/login']);
  //   });
  // }
}
