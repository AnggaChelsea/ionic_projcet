import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ChatpenjualComponent } from './components/chatpenjual/chatpenjual.component';
import { JualComponent } from './components/jual/jual.component';
import { ChatPage } from './pages/chat/chat.page';
import { LoginPage } from './pages/login/login.page';
import { PesanPage } from './pages/pesan/pesan.page';
import { ProductdetailPage } from './pages/productdetail/productdetail.page';
import { RegistrationPage } from './pages/registration/registration.page';
import {AuthguardGuard} from './helper/authguard.guard';
const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./tabs/tabs.module').then((m) => m.TabsPageModule),
  },
  {
    path: 'authentication',
    loadChildren: () =>
      import('./pages/authentication/authentication.module').then(
        (m) => m.AuthenticationPageModule
      ),
  },
  {
    path: 'products',
    loadChildren: () =>
      import('./pages/products/products.module').then(
        (m) => m.ProductsPageModule
      ),
  },
  {
    path: 'users',
    loadChildren: () =>
      import('./pages/users/users.module').then((m) => m.UsersPageModule),
  },
  {
    path: 'forgot-password',
    loadChildren: () =>
      import('./pages/forgot-password/forgot-password.module').then(
        (m) => m.ForgotPasswordPageModule
      ),
  },
  {
    path: 'registration',
    loadChildren: () =>
      import('./pages/registration/registration.module').then(
        (m) => m.RegistrationPageModule
      ),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'tabs/productsdetail/:id',
    loadChildren: () =>
      import('./pages/productdetail/productdetail.module').then(
        (m) => m.ProductdetailPageModule
      ),
  },
  {
    path: 'feed',
    loadChildren: () =>
      import('./pages/feed/feed.module').then((m) => m.FeedPageModule),
  },
  {
    path: 'pesan',
    component: PesanPage,
  },
  {
    path: 'tabs/chat',
    component: ChatPage, canActivate: [AuthguardGuard]
  },
  {
    path: 'tabs/login',
    component: LoginPage,
  },
  {
    path: 'tabs/jualcomponent',
    component: JualComponent, canActivate: [AuthguardGuard]
  },
  {
    path: 'chat/penjual',
    component: ChatpenjualComponent
  },
  {
    path: 'products/filterbyCategory/:id',
    loadChildren: () => import('./pages/productcategory/productcategory.module').then( m => m.ProductcategoryPageModule)
  },
  {
    path: 'register',
    component: RegistrationPage,
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
