import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  httpOptions = {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(private http: HttpClient) {}

  login(user: any) {
    return this.http.post<any>(
      `${environment.baseExample}user/user/login`,
      user
    );
  }
  registerUser(){
    return this.http.post<any>(
      `${environment.baseExample}users/user/register`,
      this.httpOptions
    );
  }
  getUser(user: any): Observable<any> {
    return this.http.get<any>(
      `${environment.baseExample}users/user/getUser`,
      user
    );
  }

  userIsLogin() {
    return !!localStorage.getItem('token');
  }
}
