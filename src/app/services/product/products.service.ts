import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  dataproductbycategoryid = 'https://arcane-tor-29221.herokuapp.com/';
  cobadatanya = 'https://arcane-tor-29221.herokuapp.com/products/filterbyCategory/62086f7b917ad254e651f1b5';
  constructor(private http: HttpClient) {}
  getProducts() {
    return this.http.get<any>(`${environment.baseExample}products/productfind`);
  }
  listCategory() {
    return this.http.get<any>(
      `${environment.baseExample}category/categoryList`
    );
  }
  getexmapleapi() {
    return this.http.get<any>(`${environment.baseExample}product`);
  }
  getCategoryById(id: any) {
    return this.http.get<any>(
      `${environment.baseExample}category/find_category_by_id/${id}`
    );
  }
  getProductByCategory() {
    return this.http.get<any>(
      this.cobadatanya,
    );
  }
  productcobaget() {
    return this.http.get<any>(
      ''
    );
  }
  exdatabycategory(_id: any) {
    return this.http.get<any>(
      `${this.dataproductbycategoryid}products/filterbyCategory/`
    );
  }

  addProduct(product: any) {
    return this.http.post<any>(
      `${environment.baseExample}products/addnewproduct`,
      product
    );
  }
  getInboxByUser(id: any) {
    return this.http.get<any>(
      `${environment.baseExample}inbox/getInboxByUserId/${id}`
    );
  }
}
