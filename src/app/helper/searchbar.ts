interface SearchbarCustomEvent extends CustomEvent {
  detail: SearchbarChangeEventDetail;
  target: HTMLIonSearchbarElement;
}
interface SearchbarChangeEventDetail {
  value?: string;
}
