import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JualComponent } from '../components/jual/jual.component';
import { TabsPage } from './tabs.page';
import {AuthguardGuard} from '../helper/authguard.guard';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
      },
      {
        path: 'feed',
        loadChildren: () => import('../pages/feed/feed.module').then(m => m.FeedPageModule)
      },
      {
        path: 'jualcomponent',
        component: JualComponent, canActivate: [AuthguardGuard]
      },
      {
        path: 'tab3',
        loadChildren: () => import('../pages/users/users.module').then(m => m.UsersPageModule), canActivate: [AuthguardGuard]
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
